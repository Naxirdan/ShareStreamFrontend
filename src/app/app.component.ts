import { Component, OnInit, DoCheck, ViewChild } from '@angular/core';
import { UserService} from './services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Movie } from './models/movies';
import { ImdbService } from './services/imdbServices';
import { Location } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, ImdbService]
})
export class AppComponent implements OnInit, DoCheck {
  @ViewChild('search') searchInput;
  public title: String;
  public identity: any;
  public imdbID: any;
  public movie: Movie;
  public titleM: String;
  public description: String;
  public genre: String;
  public category: String;
  public year: String;
  public creator: String;
  public duration: String;
  public poster: String;
  public URI: String;
  public uploadat: String;
  public userUp: String;
  public arrMovie: Array<any>;
  public token;
  public aux: any;
  public searchinput: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService,
    private _imdbService: ImdbService, private _location: Location) {

    this.aux = '';
    this.title = 'ShareStream';
    this.movie = new Movie(
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      ''
  );
}

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    console.log(this.identity);
  }

  ngDoCheck() {
    this.identity = this._userService.getIdentity();
  }

  showMovie(input) {
    console.log(this.searchInput['nativeElement'].value);
    if (input.key !== 'Shift' && input.key !== 'Backspace') {
      this.aux = this.aux + input.key;
    }
   this._imdbService.getAllMovies(this.searchInput['nativeElement'].value).subscribe(response => {
      this.arrMovie = [];
      response.Search.forEach(element => {
        console.log(element);
        this.arrMovie.push(element);
      });
      console.log(this.arrMovie);
    },
    error => {
      console.log(error);
    });
  }

  logout() {
    localStorage.clear();
    this.identity = null;
    this._router.navigate(['/']);
  }
  compareMovie(imdbID: any) {
    this._imdbService.compareDatabase(imdbID, localStorage.getItem('token')).subscribe(response => {
      console.log(response);
    },
    err => {
      console.log(err);
    });
  }

  cleanArr() {
    this.arrMovie = null;
    location.reload();
  }

}

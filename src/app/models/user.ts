export class User {
    constructor (
        public _id: String,
        public name: String,
        public surname: String,
        public nickname: String,
        public birthdate: String,
        public email: String,
        public password: String,
        public role: String,
        public image: String,
        public enrollat: String
    ) {}
}

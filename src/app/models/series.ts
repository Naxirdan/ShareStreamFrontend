export class Serie {
    constructor (
        title: String,
        description: String,
        genre: String,
        category: String,
        year: String,
        creator: String,
        duration: String,
        poster: String,
        uploadat: String,
        userUp: String
    ) {}
}

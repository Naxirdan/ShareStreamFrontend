export class Movie {
    constructor (
        id: String,
        imdbID: String,
        title: String,
        description: String,
        genre: String,
        category: String,
        year: String,
        creator: String,
        duration: String,
        file: String,
        URI: String,
        uploadat: String,
        userUp: String
    ) {}
}

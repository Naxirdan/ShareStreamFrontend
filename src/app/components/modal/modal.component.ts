import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    providers: []
})
export class ModalComponent {
    public title: string;
    public ver;

    constructor( private _route: ActivatedRoute, private _router: Router) {
        this.title = 'Alert!';
        this.ver = false;
    }
}

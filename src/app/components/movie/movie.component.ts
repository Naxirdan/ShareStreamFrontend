import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ImdbService } from '../../services/imdbServices';
import { Movie } from '../../models/movies';
import { Link } from '../../models/link';
import { GLOBAL } from '../../services/global';
import { MoviesService } from '../../services/movies.service';
import { UploadService } from '../../services/upload.service';
import { timeout } from 'q';

@Component({
    selector: 'app-movie',
    templateUrl: './movie.component.html',
    providers: [ImdbService, MoviesService, UploadService]
})
export class MovieComponent implements OnInit {
    @ViewChild('video') videoFile: any;
    public title: string;
    public imdbID: any;
    public movie: Movie;
    public arrMovie: Array<any>;
    public link: Link;
    public video: Array<any>;
    public token: any;
    public source: any;
    public ruta: String;
    public url: String;
    public filesUpload: Array<File>;
    idMovie: any;
    // tslint:disable-next-line:max-line-length
    constructor(private _route: ActivatedRoute, private _router: Router, private _imdbService: ImdbService, private _movieService: MoviesService, private _uploadService: UploadService) {
        this.title = 'Movie';
        this.link = new Link('', '', '', '', '');
        this.movie = new Movie('', '', '', '', '', '', '', '', '', '', '', '', '');
        this.token = localStorage.getItem('token');
        this.video = [{ video: '' }];
        this.url = GLOBAL.url;
    }

    ngOnInit() {
        this.imdbID = this._router.url.split('/');
        this.imdbID = this.imdbID[2];
        this._imdbService.compareDatabase(this.imdbID, this.token).subscribe(response => {
            if ( response.message === false) {
                this._imdbService.getAllMoviesID(this.imdbID).subscribe(resMovie => {
                    this.arrMovie = [{
                      _id: null,
                      imdbID: resMovie.imdbID,
                      title: resMovie.Title,
                      description: resMovie.Plot,
                      genre: resMovie.Genre,
                      category: resMovie.Type,
                      year: resMovie.Year,
                      creator: resMovie.Director,
                      duration: null,
                      file: resMovie.Poster,
                      uri: resMovie.Website,
                      uploadat: null,
                      userUp: 'admin'
                    }];
                    this.movie = this.arrMovie[0];
                    this.source = false;
                    console.log(resMovie);
                    console.log(this.movie);
                },
                err => {
                    console.log(err);
                });
            } else {
                this.movie = response.exist;
                console.log(this.movie);
                this.source = true;
            }
          },
          err => {
            console.log(err);
          });
    }

    verVideo() {
        console.log();
    }
    onSubmit() {
        if ( this.source ) {
            // tslint:disable-next-line:max-line-length
            const ident = JSON.parse(JSON.stringify(this.movie));
            // tslint:disable-next-line:max-line-length
            this._uploadService.makeFileRequest(this.url + 'up-video/movie/' + ident._id, [], this.filesUpload, this.token, 'video').then((resVideo: any) => {
                console.log(resVideo);
            });
        } else {
        this._imdbService.postMovie(this.movie, this.token).subscribe(resMovie => {
            console.log(resMovie);
            this.idMovie = resMovie._id;
            this.upVideo(resMovie);
          },
          err => {
            console.log(err);
          });
        }
    }

    upVideo(res: any) {
             console.log(res);
             // tslint:disable-next-line:max-line-length
             this._uploadService.makeFileRequest(this.url + 'up-video/movie/' + res.movie._id, [], this.filesUpload, this.token, 'video').then((resVideo: any) => {
                console.log(resVideo);
             });
    }

    fileChangeEvent(fileInput: any) {
        this.filesUpload =  <Array<File>>fileInput.target.files;
        console.log(this.filesUpload);
    }

    deleteMovie(id) {
        this._movieService.deleteMovie(this.token, id).subscribe(resDelete => {
            console.log(resDelete);
            location.reload();
        },
        errDelete => {
            console.log(errDelete);
        });
    }

}

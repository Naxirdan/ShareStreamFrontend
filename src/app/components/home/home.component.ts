import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../models/movies';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    providers: [MoviesService]
})

export class HomeComponent implements OnInit {
    public title: String;
    public movie: Movie;
    public listMovie: Array<Movie>;
    public usuarioDel: String;

    // tslint:disable-next-line:max-line-length
    constructor(private _route: ActivatedRoute, private _router: Router, private _movieService: MoviesService) {
        this.title = 'Home';
        this.movie = new Movie(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
    }

    ngOnInit(): void {
        this._movieService.getAllMovies(localStorage.getItem('token')).subscribe(response => {
            this.listMovie = [];
            response.message.forEach(eleMovie => {
                this.movie = eleMovie;
                this.listMovie.push(this.movie);
                console.log(this.movie);
            });
        },
        error => {
            console.log(error);
        });
    }
}

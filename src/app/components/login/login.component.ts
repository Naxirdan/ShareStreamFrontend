import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserLogin } from '../../models/userLogin';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    providers: [UserService]
})
export class LoginComponent implements OnInit {
    public title: string;
    public user: User;
    public userLogin: UserLogin;
    public status: String;
    public errorDetails: String;
    public token;

    constructor(private _route: ActivatedRoute, private _router: Router,  private _userService: UserService) {
        this.title = 'Login';
        this.user = new User('',
            '',
            '',
            '',
            '',
            '',
            '',
            'ROLE_USER',
            '',
            '');
        this.userLogin = new UserLogin('', '', '', '');
    }

    ngOnInit(): void {
        if (this._userService.getIdentity()) {
            this._router.navigate(['/admin']);
        }
    }

    onSubmit(form) {
        this._userService.login(this.userLogin).subscribe(response => {
            this.user = response.user;

            if (!response.user || !response.user._id) {
                this.status = 'Error';
                this.errorDetails = 'User/email or password incorrect, try again!';
            } else {
                if (response.user.role === 'ADMIN_ROLE') {
                    this.status = 'Success';
                    localStorage.setItem('user', JSON.stringify(this.user));
                    this.errorDetails = 'Login successful, enjoy!!';
                    this.getToken();
                } else {
                    this.status = 'Error';
                    this.errorDetails = 'Este usuario no es administrador del sistema';
                }
            }
        },
        error => {
                let errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = 'Error';
                    this.errorDetails = 'User/email or password incorrect, try again!';
                }
        });
    }

    getCounter() {
        this._userService.getCounter(localStorage.getItem('token')).subscribe(response => {
            localStorage.setItem('stats', JSON.stringify(response));
            this.status = 'Success';
            this.errorDetails = 'Login successful, enjoy!!';
            this._router.navigate(['/admin']);
        },
        error => {
            console.log(error);
        });
    }

    getToken() {
        this._userService.login(this.userLogin, 'true').subscribe(response => {
            this.token = response.token;

            if (this.token.length <= 0) {
                this.status = 'error';
                this.errorDetails = 'Error al generar el token';

            } else {
                localStorage.setItem('token', this.token);
                this.getCounter();
            }
        },
        error => {
                let errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = 'Error';
                    this.errorDetails = 'User/email or password incorrect, try again!';
                }
        });
    }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Location } from '@angular/common';


import * as $ from 'jquery';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    providers: [UserService]
})

export class AdminComponent implements OnInit {
    public title: String;
    public user: User;
    public listUser: Array<User>;
    public usuarioDel: String;
    public updateUser: User;
    public userArr: Array<any>;
    public page: number;
    public hidenModal: boolean;
    public totalUsers: number;
    // tslint:disable-next-line:max-line-length
    constructor(private _route: ActivatedRoute, private _router: Router, private _UserService: UserService, private location: Location) {
        this.title = 'Administration';
        this.page = 1;
        this.user = new User(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        this.updateUser = new User(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        this.userArr = [{
            name: '',
            surname: '',
            nickname: '',
            birthdate: '',
            email: '',
            password: '',
            role: ''
        }];
    }

    ngOnInit(): void {
      this._UserService.getAllUsers(localStorage.getItem('token')).subscribe(response => {
          this.totalUsers = response.total;
        this.listUser = [];
        response.users.forEach(element => {
            this.user = element;
            this.listUser.push(this.user);
        });
        },
        err => {
            console.log(err);
        });
    }

    moreUsers() {
        this.page = this.page + 1;
        this._UserService.getAllUsers(localStorage.getItem('token'), this.page).subscribe(response => {
            response.users.forEach(element => {
                this.user = element;
                this.listUser.push(this.user);
            });
            },
            err => {
                console.log(err);
            });
    }

    getData(id: any): void {
        this.usuarioDel = id;
    }
    getUpd(user: User): void {
        this.user = user;
    }

    deleteUser(): void {
        this._UserService.deleteUser(localStorage.getItem('token'), this.usuarioDel).subscribe(response => {
            location.reload();
        console.log(response);
        },
        err => {
            console.log(err);
        });
    }

    updUser() {
        if (this.updateUser.name !== '') {
            this.userArr[0].name = this.updateUser.name;
        } else {
            this.userArr[0].name = this.user.name;
        }
        if (this.updateUser.surname !== '') {
            this.userArr[0].surname = this.updateUser.surname;
        } else {
            this.userArr[0].surname = this.user.surname;
        }
        if (this.updateUser.nickname !== '') {
            this.userArr[0].nickname = this.updateUser.nickname;
        } else {
            this.userArr[0].nickname = this.user.nickname;
        }
        if (this.updateUser.birthdate !== '') {
            this.userArr[0].birthdate = this.updateUser.birthdate;
        } else {
            this.userArr[0].birthdate = this.user.birthdate;
        }
        if (this.updateUser.email !== '') {
            this.userArr[0].email = this.updateUser.email;
        } else {
            this.userArr[0].email = this.user.email;
        }
        if (this.updateUser.password !== '') {
            this.userArr[0].password = this.updateUser.password;
        } else {
            this.userArr[0].password = this.user.password;
        }
        if (this.updateUser.role !== '') {
            this.userArr[0].role = this.updateUser.role;
        } else {
            this.userArr[0].role = this.user.role;
        }

        this._UserService.updateUser(this.userArr[0], localStorage.getItem('token'), this.user._id)
        .subscribe(response => {
            location.reload();
          },
          err => {
            console.log(err);
        });
      }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
//import { User } from '../models/user';
import { Movie } from '../models/movies';
import { GLOBAL } from './global';

@Injectable()
export class MoviesService {
    public url: String;
    public identity: any;

    constructor(private _http: HttpClient) {
        this.url = GLOBAL.url;
    }

    getAllMovies(token: any): Observable<any> {
        let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);

        return this._http.get(this.url + 'getallmovies',  {headers: headers});
    }

    newContent(token, id, video): Observable<any> {
        let params = JSON.stringify(video);
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

        return this._http.post(this.url + 'up-video/movie/' + id, params, {headers: headers}) ;
    }

    deleteMovie(token, id): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

        return this._http.delete(this.url + 'deletemovie/' + id, {headers: headers}) ;
    }
}

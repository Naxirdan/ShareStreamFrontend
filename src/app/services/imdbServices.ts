import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable()
export class ImdbService {
    public url: String;
    public identity: any;

    constructor(private _http: HttpClient) {
        this.url = GLOBAL.url;
    }

    getAllMovies(imdbID: String): Observable<any> {
        let headers = new HttpHeaders().set('Content-type', 'application/json');
        return this._http.get('http://www.omdbapi.com/?apikey=10e37d7e&s=' + imdbID);
    }
    getAllMoviesID(imdbID: String): Observable<any> {
        console.log(imdbID);
        let headers = new HttpHeaders().set('Content-type', 'application/json');
        return this._http.get('http://www.omdbapi.com/?apikey=10e37d7e&i=' + imdbID);
    }
    postMovie(movie, token): Observable<any> {
        let headers = new HttpHeaders().set('Authorization', token).set('Content-type', 'application/json');
        return this._http.post('http://ec2-18-217-133-241.us-east-2.compute.amazonaws.com:9000/api/newmovie', movie, {headers: headers} );
    }
    compareDatabase(imdbID, token): Observable<any> {
        let headers = new HttpHeaders().set('Authorization', token).set('Content-type', 'application/json');
        return this._http.get(this.url + 'get-movie-imdb/' + imdbID, {headers: headers});
    }
}
